
// Minimum product of k integers

import java.io.*;
class Demo{

	static int minimumProduct(int arr[], int k){
	
		int product = 1;

		for(int i=0; i<k; i++){
		
			product = product*arr[i];
		}

		return product;
	}

	static void merge(int arr[], int start, int mid, int end){
	
		int ele1 = mid-start+1;
		int ele2 = end - mid;

		int arr1[] = new int[ele1];
		int arr2[] = new int[ele2];

		for(int i=0; i<ele1; i++){
		
			arr1[i] = arr[start+i];
		}

		for(int j=0; j<ele2; j++){
		
			arr2[j] = arr[mid+1+j];
		}

		int itr1 =0;
		int itr2 =0;
		int itr3 = start;

		while(itr1 < ele1 && itr2 < ele2){
		
			if(arr1[itr1] < arr2[itr2]){
			
				arr[itr3] = arr1[itr1];
				itr1++;
			}else{
			
				arr[itr3] = arr2[itr2];
				itr2++;
			}
			itr3++;
		}

		while(itr1 < ele1){
		
			arr[itr3] = arr1[itr1];
			itr1++;
			itr3++;
		}

		while(itr2 < ele2){
		
			arr[itr3] = arr2[itr2];
			itr2++;
			itr3++;
		}
	}

	static void mergeSort(int arr[], int start, int end){
	
		if(start < end){
		
			int mid = (start + end)/2;

			mergeSort(arr,start,mid);
			mergeSort(arr,mid+1,end);
			merge(arr,start,mid,end);
		}
	}
	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		mergeSort(arr,0,arr.length-1);
		
		System.out.println("Enter the number of k ");
		int k = Integer.parseInt(br.readLine());

		int ret = minimumProduct(arr,k);
		System.out.println("The  minimum produc of k integers is : "+ ret);
	}
}
