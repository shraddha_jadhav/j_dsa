
// Remove an element at specific Index from an array
// i/p - arr[] = {1,2,3,4,5} , index = 3
// o/p - arr[] = {1,2,3,5}

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the array elements");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i]+ " ");
		}
		System.out.println();

		System.out.println("Enter the index which is remove:");
		int idx = Integer.parseInt(br.readLine());

		int arr2[] = new int[size-1];
		if(idx < arr.length || idx >= 0){
			int k = 0;
			for(int i=0; i<arr.length; i++){
			
				if(i==idx){
				
					continue;
				}

				arr2[k] = arr[i];
				k++;
			}

			System.out.println("After removing the element of that index the array is...");

                	for(int i=0; i<arr2.length; i++){
			
				System.out.print(arr2[i] + " ");
			}
			System.out.println();
		}else{
		
			System.out.println("Invalid Index");
		}
	}
}
