
// Find Subarray with given sum | set 1 (Non-negative numbers)

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements (Positive number) ");

		int num;
		for(int i=0; i<arr.length; i++){
		
			num = Integer.parseInt(br.readLine());

			if(num >=0){
				arr[i] = num;
			}else{
			
				System.out.println("plz enter valid number");
				num = Integer.parseInt(br.readLine());
				arr[i] = num;
			}
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		System.out.println("Enter the sum of subarray which is found...");
		int Sum = Integer.parseInt(br.readLine());

		int i=0;
		int j=0;
		int flag = 0;

		for(i=0; i<arr.length; i++){
		
			int add = 0;
			for(j=i; j<arr.length; j++){
		
				add = add + arr[j];
				if(add == Sum){
					flag = 1;
					break;
				}
			}
			if(add==Sum)
				break;
		}

		if(flag==1){
		
			System.out.println("The Sum found between indexes "+ i +" and "+ j);
		}else{
		
			System.out.println("No such a subarray found");
		}
	}
}
