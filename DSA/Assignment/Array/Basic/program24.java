
// Ceil and Floor:-
// eg-
//     i/p :- arr[] = {5,6,8,9,6,5,5,6}
//            x = 7
//     o/p :- 6 8 [floor of 7 is 6 and ceil of 7 is 8]

import java.io.*;
class Demo{

	static void CFelement(int arr[], int num){
		
		int min = arr[0];
		int max = arr[arr.length-1];

		for(int i=0; i<arr.length; i++){
		
			if(num==arr[i]){
			
				System.out.println("ceil and floor are both same of " + num + "i.e " + arr[i]);
				break;
				
			}else if( i >= 1 && num < arr[i] && num > arr[i-1]){

                                System.out.println("floor of "+ num +" is " + arr[i-1] );
                                System.out.println("ceil of " + num + " is "+ arr[i]);
				break;
                        
			}else if(min > num){
			
				System.out.println("floor of "+ num +" is not possible" );
                                System.out.println("ceil of " + num + " is "+ arr[i]);
				break;
			
			}else if(max < num){
			
				System.out.println("floor of "+ num +" is " + arr[arr.length-1] );
                                System.out.println("ceil of " + num + " is not possible");
				break;
			}
		}
	}

	static void Sorting(int arr[]){
	
		for(int i=1; i<arr.length; i++){
		
			int val = arr[i];
			int j = i-1;

			while(j>=0 && arr[j] > val){
			
				arr[j+1] = arr[j];
				j--;
			}

			arr[j+1] = val;
		}
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		Sorting(arr);
		
		System.out.println("Enter the element for finding their Ceil and floor");
		int num = Integer.parseInt(br.readLine());

		CFelement(arr,num);
	}
}
