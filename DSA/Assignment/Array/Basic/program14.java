

import java.io.*;
class Demo{

	static void counting(int arr[]){
	
		int max = arr[0];
		for(int i=1; i<arr.length; i++){
		
			if(arr[i] > max)
				max = arr[i];
		}

		int countArr[] = new int[max+1];

		for(int i=0; i<arr.length; i++){
		
			countArr[arr[i]]++;
		}

		int maximum = 0;
		int num = 0;
		for(int i=countArr.length-1; i>=0; i--){
		
			if(countArr[i] >= maximum){
				maximum = countArr[i];
				num = i;
			}
		}

		System.out.println("The maximum repeating number is " + num );
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

//		sorting(arr);

		counting(arr);
	}
}
