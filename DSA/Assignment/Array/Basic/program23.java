// Find the smallest and second smallest element in an array
// eg-
//     i/p :-  arr[] ={2,4,3,5,6}
//     o/p :-  2,3

import java.io.*;
class Demo{

	static void smallestEle(int arr[]){
	
		int min1= arr[0];
		int min2 = arr[1];

		for(int i=1; i<arr.length; i++){
		
			if(min1 > arr[i] || min2 > arr[i]){
			
				min2 = min1;
				min1 = arr[i];
			}
		}
		if(min1 < min2)
			System.out.println("The 1st and 2nd smallest elements are "+ min1 + " and "+ min2);
		else
			System.out.println("The 1st and 2nd smallest elements are "+ min2 + " and "+ min1);
	}
	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		smallestEle(arr);
	}
}
