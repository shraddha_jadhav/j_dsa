
// Elements in the range 
// check if the array contains all the element in given range prsent or not

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");;
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the array elements");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i] + " ");
		}
		System.out.println();

		System.out.println("Enter the range...");

		int range1 = Integer.parseInt(br.readLine());
		int range2 = Integer.parseInt(br.readLine());
		System.out.println("All elements are present in betwen the range ?");
		int flag = 0;
		while(range1 <= range2){
		
			 flag = 0;

			for(int i=0; i<arr.length; i++){
			
				if(range1==arr[i]){
				
					flag = 1;
				}
			}
			if(flag==0){
			
				System.out.println("No");
				break;
			}

			range1++;
		}

		if(flag==1){
		
			System.out.println("yes");
		}


	}
}
