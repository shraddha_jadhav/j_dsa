
// Even occurring elements
// i/p = arr[]={11,12,14,12,15,10,15}
// o/p = 12 15

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of the array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the array elements");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i] + " ");
		}
		System.out.println();

		System.out.println("the numbers which is occured even times are");

		int flag = 0;
		for(int i=0; i<arr.length; i++){
		
			int temp = arr[i];
			int count = 1;
			for(int j=i+1; j<arr.length; j++){
			
				if(temp==arr[j])
					count++;
			}
			
			if(count%2==0){
				flag = 1;
				System.out.print(arr[i]+ " ");
			}
	
		}
		
		if(flag==0){
		
			System.out.println("No such element is present");
		}
	}
}
