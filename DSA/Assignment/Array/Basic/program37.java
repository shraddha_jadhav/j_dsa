
// Move all negative numbers to begining and positive to end with constant extra space.
// eg-
// 	i/p :- arr[] = {-12,11,-13,-5,6,-7,5,-3,-6}
// 	o/p :- -12 -13 -5 -7 -3 -6 11 6 5

import java.io.*;
class Demo{

	static void Move(int arr[]){
	
		for(int i=0; i<arr.length; i++){
		
			for(int j=0; j<arr.length-i-1; j++){
			
				if(arr[j] >= 0 && arr[j+1] < 0){
				
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1]= temp;
				}
			}
		}

		System.out.println("After moving the positive and negative numbers... ");

		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		Move(arr);
	}
}
