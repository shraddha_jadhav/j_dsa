
// Product of the array

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("enter array elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");

		int product = 1;
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i] + " ");
			product *= arr[i];
		}
		System.out.println();
	
		System.out.println("The product of the array = " + product);
	}
}
