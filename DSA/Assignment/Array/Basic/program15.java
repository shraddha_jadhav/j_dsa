
// Sum of distinct element
// eg-
// 	i/p :- arr[] = {5,5,5,5,5}
// 	o/p :- 5

import java.io.*;
class Demo{

	static int eleSum(int arr[]){
	
		int max = arr[0];

		for(int i=0; i<arr.length; i++){
		
			if(arr[i] > max)
				max = arr[i];
		}
		int countArr[] = new int[max+1];

		for(int i=0; i<arr.length; i++){
		
			countArr[arr[i]]++;
		}

		int sum = 0;
		for(int i=0; i<countArr.length; i++){
		
			if(countArr[i] > 0)
				sum = sum + i;

		}

		return sum;
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		int ret = eleSum(arr);
		System.out.println("The sum of distict elements is : " + ret);
	}
}
