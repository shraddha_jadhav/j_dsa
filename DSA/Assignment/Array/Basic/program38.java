
// Immediate Smaller Elements
// eg-
// 	i/p :- arr[] = {4,2,1,5,3}
// 	o/p :- 2 1 -1 3 -1

import java.io.*;
class Demo{

	static void Smaller(int arr[]){
	
		for(int i=0; i<arr.length-1; i++){
		
			if(arr[i] > arr[i+1]){
			
				arr[i] = arr[i+1];
			}else{
			
				arr[i] = -1;
			}
		}
		arr[arr.length-1] = -1;

		System.out.println("The  immediate smaller numbers are...");

		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		Smaller(arr);
	}
}
