
// Countries at war
// eg- 
// 	i/p:- A[] = {2,2}
// 	      B[] = {5,5}
//	o/p :- B  [B countri is winner] 

import java.io.*;
class Demo{

	static void War(int A[], int B[]){
	
		int count1 = 0;
	        int count2 = 0;

		for(int i=0; i < A.length; i++){
		
			if(A[i] > B[i]){
			
				count1++;
			}

			if(B[i] > A[i]){
			
				count2++;
			}
		}

		if(count1 > count2)
			System.out.println("Country 'A' is Win the war");
		else if(count1 < count2)
			System.out.println("Country 'B' is win the war");
		else
			System.out.println("All soldier of Country 'A' and 'B' are killed");

	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int A[] = new int[size];

		System.out.println("Enter Soldier of country A");

		for(int i=0; i<A.length; i++){
		
			A[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The soldier of country A is...");
		
		for(int x : A){
		
			System.out.print(x + " ");
		}
		System.out.println();

		int B[] = new int[size];

                System.out.println("Enter soldier of country B");

                for(int i=0; i<A.length; i++){

                        B[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The soldier of country B is...");

                for(int y : B){

                        System.out.print(y + " ");
                }
                System.out.println();

		War(A,B);
	}
}
