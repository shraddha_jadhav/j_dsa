
// Count number of elements between two given elements in the array
// eg-
// 	i/p :- arr[] = {4,2,1,10,6}, num1=4, num2=6
// 	o/p :- 3

import java.io.*;
class Demo{

	static int Count(int arr[], int num1, int num2){
	
		int idx1 = 0;
		int idx2 = 0;

		for(int i=0; i<arr.length; i++){
		
			if(arr[i]==num1)
				idx1 = i;

			if(arr[i]==num2)
				idx2 = i;
		}

		return idx2-idx1-1;
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		System.out.println("Enter the num1 and num2 in the given array:");
		int num1 = Integer.parseInt(br.readLine());
		int num2 = Integer.parseInt(br.readLine());

		int ret = Count(arr, num1, num2);
		System.out.println("The count of between the elements of " +num1 +" and " + num2+ " are :"+ ret);
	}
}
