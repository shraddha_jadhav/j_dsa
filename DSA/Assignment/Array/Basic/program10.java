
// max odd sum
// i/p - arr[]={4,-3,3,-5}
// o/p - 7

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int i=0; i<arr.length; i++){
		 
			System.out.print(arr[i] + " ");
		}
		System.out.println();

		// largest odd sum

		for(int i=0; i<arr.length; i++){
		
			int maxidx = i;

			for(int j=i+1; j<arr.length; j++){
			
				if(arr[maxidx]<arr[j]){
				
					maxidx = j;
				}
			}

			int temp = arr[maxidx];
			arr[maxidx] = arr[i];
			arr[i] = temp;
		}

		for(int i=1; i<arr.length; i++){
		
			arr[i] = arr[i] + arr[i-1];
		}

		for(int i=arr.length -1 ; i>=0; i--){
		
			if(arr[i]%2==1){
			
				System.out.println("largest odd sum : "+ arr[i]);
				break;
			} 
		}
	}
}
