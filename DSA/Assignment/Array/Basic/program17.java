
// Product of maximum in first array and minimum in second
// i/p - arr1[] = {5,7,9,3,6,2}
//       arr2[] = {1,2,6,-1,0,9}
// o/p = -9

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of 1st array");
		int size1 = Integer.parseInt(br.readLine());
		int arr1[] = new int[size1];

		System.out.println("Enter size of the 2nd array");
		int size2 = Integer.parseInt(br.readLine());
		int arr2[] = new int[size2];

		System.out.println("Enter the elements of 1st array");
		for(int i=0; i<arr1.length; i++){
		
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the elements of the 2nd array");
		for(int i=0; i<arr2.length; i++){
		
			arr2[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("1st array is...");
		int max = 0;
		for(int i=0; i<arr1.length; i++){
		
			System.out.print(arr1[i] + " ");
			if(max < arr1[i])
				max = arr1[i];
		}
		System.out.println();

		int min = arr2[0];
		System.out.println("2nd array is...");
		for(int i=0; i<arr2.length; i++){
		
			System.out.print(arr2[i] + " ");
			if(min > arr2[i])
				min = arr2[i];
		}
		System.out.println();

		System.out.println("The product of maximum element in 1st array and minimum number of 2nd array is "+max*min );
	}
}
