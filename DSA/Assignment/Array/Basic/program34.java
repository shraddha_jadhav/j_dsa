
// Print an array in pendulum arrangement
// eg-
// 	i/p :- arr[] = {1,3,2,5,4}
// 	o/p :- 5 3 1 2 4

import java.io.*;
class Demo{

	static void Pendulum(int arr[]){
	
		int arr2[] = new int[arr.length];

		int mid = arr.length/2;
		int left = mid-1;
		int right = mid+1;
		int count = 0;

		for(int i=0; i<arr.length; i++){
		
			if(i==0){
			
				arr2[mid] = arr[i];
			}else{
			
				if(count%2==0){
				
					arr2[left] = arr[i];
					left--;
				}else{
				
					arr2[right] = arr[i];
					right++;
				}
			}
			count++;
		}

		System.out.println("The  array is print in pendulum form : ");
		for(int i=0; i<arr2.length; i++){
		
			System.out.print( arr2[i] + " ");
		}
		System.out.println();
	}

	static void merge(int arr[], int start, int mid, int end){
	
		int ele1 = mid-start+1;
		int ele2 = end - mid;

		int arr1[] = new int[ele1];
		int arr2[] = new int[ele2];

		for(int i=0; i<ele1; i++){
		
			arr1[i] = arr[start+i];
		}

		for(int j=0; j<ele2; j++){
		
			arr2[j] = arr[mid+1+j];
		}

		int itr1 =0;
		int itr2 =0;
		int itr3 = start;

		while(itr1 < ele1 && itr2 < ele2){
		
			if(arr1[itr1] < arr2[itr2]){
			
				arr[itr3] = arr1[itr1];
				itr1++;
			}else{
			
				arr[itr3] = arr2[itr2];
				itr2++;
			}
			itr3++;
		}

		while(itr1 < ele1){
		
			arr[itr3] = arr1[itr1];
			itr1++;
			itr3++;
		}

		while(itr2 < ele2){
		
			arr[itr3] = arr2[itr2];
			itr2++;
			itr3++;
		}
	}

	static void mergeSort(int arr[], int start, int end){
	
		if(start < end){
		
			int mid = (start + end)/2;

			mergeSort(arr,start,mid);
			mergeSort(arr,mid+1,end);
			merge(arr,start,mid,end);
		}
	}
	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		mergeSort(arr,0,arr.length-1);
		
		Pendulum(arr);
	}
}
