
// Exceptionally odd
// eg-
//    i/p :- arr[] = {1,2,3,2,3,1,3}
//    o/p :- 3 (3 occures three times) 

import java.io.*;
class Demo{

	static void oddOcc(int arr[]){
	
		for(int i=0; i<arr.length; i++){
		
			int count = 0;
			for(int j=i; j<arr.length; j++){
			
				if(arr[i]==arr[j]){
				
					count++;
				}
			}

			if(count%2==1){
			
				System.out.println(arr[i] + " occurs in odd times i.e " + count + " times");
				break;
			}
		}
	}
	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		oddOcc(arr);
	}
}
