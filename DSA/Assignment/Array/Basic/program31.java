
// Sum of f(a[i],a[j]) over all pairs in an array of n integers.

import java.io.*;
class Demo{

	static int fSum(int arr[]){
	
		int sum = 0;

		for(int i=1; i<arr.length; i++){
		
			for(int j=i+1; j<arr.length; j++){
			
				if(arr[j]-arr[i] < 1)
					sum = sum + (arr[j] - arr[i]);
				//else
				//	sum = sum + 0;
			}
		}

		return sum;
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		int ret = fSum(arr);
		System.out.println("Sum of f(a[i],a[j]) over all pairs : "+ ret);
	}
}
