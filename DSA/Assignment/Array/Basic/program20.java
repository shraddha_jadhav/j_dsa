
// check if pair with given sum exist in array (Two Sum).

import java.io.*;
class Demo{

	static boolean SumEle(int arr[], int sum){
	
		for(int i=0; i<arr.length; i++){
		
			for(int j=i+1; j<arr.length; j++){
			
				if(arr[i]+arr[j]==sum)
					return true;
			}
		}

		return false;
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		System.out.println("Enter the given Sum");
		int sum = Integer.parseInt(br.readLine());

		boolean ret = SumEle(arr,sum);

		if(ret==true)
			System.out.println("The given sum is exist");
		else
			System.out.println("The given sum is not exist");
	}
}
