
// repalce all 0's with 5
// ex -
// 	I/P = 1004
// 	O/P = 1554

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Integer value");
		int num = Integer.parseInt(br.readLine());

		int x = 1;
		int temp = num;
		while(num != 0){
		
			if(num%10==0){
			
				int rem = x *5;
				temp = temp + rem;
			}

			x = x * 10;

			num = num/10;
		}

		System.out.println("Output = " + temp);
	}
}
