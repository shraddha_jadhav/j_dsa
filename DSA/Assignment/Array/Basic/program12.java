
// First and last occurrences of X
// i/p-  arr[]={1,3,3,4}, X=3
// o/p-  1,2

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();	

		System.out.println("Enter the element...");
		int X = Integer.parseInt(br.readLine());

		int firstOcc = 0;
		int lastOcc = 0;
		int flag = 0;

		for(int i=0; i<arr.length; i++){
		
			if(X==arr[i]){
				firstOcc = i;
				flag = 1;
				break;
			}

		}

		for(int i=0; i<arr.length; i++){
		
			if(X==arr[i])
				lastOcc = i;
		}

		if(flag==1){
			System.out.println("first Occurence of " + X + " is " + firstOcc);
			System.out.println("Last Occurence of "+ X + " is "+ lastOcc);
		}else{
		
			System.out.println("Element "+ X + " is not present in the given array");
		}
	}
}
