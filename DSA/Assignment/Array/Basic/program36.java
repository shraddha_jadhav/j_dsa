
// find peak element which is not smaller than its nighbors
// eg-
// 	i/p :- arr[] = {5,10,20,15}
// 	o/p :- 20

import java.io.*;
class Demo{

	static void PeakEle(int arr[]){

		System.out.println("The  peak elements is... ");	
		if(arr[0] > arr[1]){
		
			System.out.print(arr[0] + " ");
		
		}else if(arr[arr.length-1] >arr[arr.length-2]){
		
			System.out.print(arr[arr.length-1] + " ");
		}else{
		
		
			for(int i=1; i<arr.length-1; i++){
			
				if(arr[i] > arr[i-1] && arr[i] > arr[i+1]){
				
					System.out.print(arr[i] + " ");
				}
			}
		}
		System.out.println();
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		PeakEle(arr);
	}
}
