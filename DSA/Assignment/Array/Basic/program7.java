
// form largest number from digits
//i/p = arr[] = {9,0,1,3,0}
//o/p = 93100

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter the size of the array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the array elements between the range 0-9 ");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i] + " ");
		}
		System.out.println();

		for(int i=0; i<arr.length; i++){
		
			int maxIdx = i;
			for(int j = i+1; j<arr.length; j++){
			
				if(arr[maxIdx]<arr[j])
					maxIdx = j;
			}

			int temp = arr[i];
			arr[i] = arr[maxIdx];
			arr[maxIdx] = temp;
		}

		System.out.println("The largest number is...");
                for(int i=0; i<arr.length; i++){

                        System.out.print(arr[i]);
                }
		System.out.println();

	}
}
