
// find minimum and maximum element in an array-
//Given an array arr of size N of integers. Your task is to
//find minimum and maximum elements in the array
//Time Complexity = O(n)
//Space Complexity = O(1)

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int N = Integer.parseInt(br.readLine());

		int arr[] = new int[N];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();	

		int min = arr[0];
		int max = arr[0];

		for(int i = 0; i<arr.length; i++){
		
			if(arr[i] < min){
			
				min = arr[i];
			}

			if(arr[i] > max){
			
				max = arr[i];
			}
		}

		System.out.println("The minimum and maximum elements are " + min + " and " + max);

	}
}
