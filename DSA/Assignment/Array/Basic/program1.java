
// Search an element in an array-
//Given an integer array and another integer element. The task is to 
//find if the given  element is present in the array return index otherwise not.
//Time Complexity = O(n)
//Space Complexity = O(1)

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();	

		System.out.println("Enter search element");
		int search = Integer.parseInt(br.readLine());

		int i=0;
		int flag = 0;

		for(i=0; i<arr.length; i++){
		
			if(search==arr[i]){
			
				flag = 1;
				break;
			}
		}

		if(flag==0){
		
			System.out.println("Element is not found");
		}else{
		
			System.out.println("Element found at index : " + i);
		}
	}
}
