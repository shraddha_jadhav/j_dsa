
// Remove Duplicates from unsorted array
// eg-
// 	i/p :- arr[] = {1,2,3,1,4,2}
// 	o/p :- 1 2 3 4

import java.io.*;
class Demo{

	static void RemoveDuplicates(int arr[]){
	
		int min = 0;
		for(int i=0; i<arr.length; i++){
		
			if(min > arr[i])
				min = arr[i];
		}

		if(min < 0){
		
			min = -min;

			for(int i=0; i<arr.length; i++){
			
				arr[i] = arr[i] + min;
			}
		}

		int max = 0;
		for(int i=0; i<arr.length; i++){
		
			if(max < arr[i])
				max = arr[i];
		}

		int countArr[] = new int[max+1];

		for(int i=0; i<arr.length; i++){
		
			countArr[arr[i]]++;
		}

		System.out.println("After removing Duplicate elements :");
		for(int i=0; i<countArr.length; i++){
		
			if(countArr[i] > 0)
				System.out.print(i + " ");
		}
		System.out.println();
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		RemoveDuplicates(arr);
	}
}
