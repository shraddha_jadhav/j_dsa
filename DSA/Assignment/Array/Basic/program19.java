
// Find common elements in three sorted array

import java.io.*;
class Demo{

	static void Sorted(int arr[]){
	
		for(int i=0; i<arr.length; i++){
		
			for(int j=0; j<arr.length-i-1; j++){
			
				if(arr[j] > arr[j+1]){
				
					int temp = arr[j+1];
					arr[j+1] = arr[j];
					arr[j] = temp;
				}
			}
		}
		System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of 1st array");

		int size1 = Integer.parseInt(br.readLine());

		int arr1[] = new int[size1];

		System.out.println("Enter 1st array Elements");

		for(int i=0; i<arr1.length; i++){
		
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter size of 2nd array");
		int size2 = Integer.parseInt(br.readLine());

                int arr2[] = new int[size2];

                System.out.println("Enter 2nd array Elements");

                for(int i=0; i<arr2.length; i++){

                        arr2[i] = Integer.parseInt(br.readLine());
                }

		System.out.println("Enter size of 3rd array");
		int size3 = Integer.parseInt(br.readLine());

                int arr3[] = new int[size3];

                System.out.println("Enter 3rd array Elements");

                for(int i=0; i<arr3.length; i++){

                        arr3[i] = Integer.parseInt(br.readLine());
                }

  		Sorted(arr1);
		Sorted(arr2);
		Sorted(arr3);

		// common elements in three array
		System.out.println("The common elements in all 3 arrays are");
		for(int i=0; i<arr1.length; i++){
		
			for(int j=0; j<arr2.length; j++){
			
				if(arr1[i]==arr2[j] ){
				
					for(int k=0; k<arr3.length; k++){
					
						if(arr1[i]==arr3[k]){
						
							System.out.print(arr1[i] + " ");
						}

						if(arr1[i] < arr3[k])
							break;
					}
				}

				if(arr1[i] < arr2[j])
					break;
			}
		}
		System.out.println();
	}
}
