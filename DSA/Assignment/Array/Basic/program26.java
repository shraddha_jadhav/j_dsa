
// Positive and negative elements-
// Given an array arr[] containing equal number of positive and negative elements,
// arrange the array such that every positive element is followed by a negative element
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		//

		int arr2[] = new int[size];

		int pos = 0;
		int neg = 1;

		for(int i=0; i<arr.length; i++){
		
			if(arr[i] >= 0){
			
				arr2[pos] = arr[i];
				pos += 2;
			}else{
			
				arr2[neg] = arr[i];
				neg +=2;
			}
		}

		for(int i=0; i<arr2.length; i++){
		
			System.out.print(arr2[i] + " ");
		}
		System.out.println();
	}
}
