
// Largest element in array-
//Given an array A[] of size n. The task is to 
//find the largest element in it.
//Time Complexity = O(n)
//Space Complexity = O(1)

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int n = Integer.parseInt(br.readLine());

		int A[] = new int[n];

		System.out.println("Enter array Elements");

		for(int i=0; i<A.length; i++){
		
			A[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : A){
		
			System.out.print(x + " ");
		}
		System.out.println();	

		int max = A[0];

		for(int i=1; i<A.length; i++){
		
			if(A[i] > max){
			
				max = A[i];
			}
		}

		System.out.println("The largest element of a given array is " + max);
	}
}
