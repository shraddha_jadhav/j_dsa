// first element to occure k times.
// eg-
//     i/p :- k = 2
//            arr[] = {1,7,4,3,4,8,7}
//     o/p :-  4

import java.io.*;
class Demo{

	static void firstOcc(int arr[], int k){
	
		int max= arr[0];

		for(int i=1; i<arr.length; i++){
		
			if(max<arr[i])
				max = arr[i];
		}

		int countArr[] = new int[max+1];

		for(int i=0; i<arr.length; i++){
		
			countArr[arr[i]]++;

			if(countArr[arr[i]]==k){
			
				System.out.println("the 1st element which occurred " + k + " times is " + arr[i]);
				return;
				
			}
		}

		System.out.println("There is no such element");
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		System.out.println("Enter the occurence (i.e k)");
		int k = Integer.parseInt(br.readLine());

		firstOcc(arr,k);
	}
}
