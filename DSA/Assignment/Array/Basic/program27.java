
// Count pair sum
// eg-
// 	i/p - arr1[] = {1,3,5,7}
// 	      arr2[] = {2,3,5,8}
// 	      sum = 10
// 	o/p - 2

import java.io.*;
class Demo{

	static void countPair(int arr1[], int arr2[], int sum){
	
		int count = 0;
		for(int i=0; i<arr1.length; i++){
		
			for(int j=0; j<arr2.length; j++){
			
				if(arr1[i]+arr2[j]==sum)
					count++;
			}
		}

		System.out.println(count + " pairs are exist their sum is " + sum);
	}

	static void Sorting(int arr[]){
	
		for(int i=1; i<arr.length; i++){
		
			int val = arr[i];
			int j = i-1;

			while(j>=0 && arr[j]>val){
			
				arr[j+1] = arr[j];
				j--;
			}
			arr[j+1] = val;
		}
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr1[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr1.length; i++){
		
			arr1[i] = Integer.parseInt(br.readLine());
		}
		
		Sorting(arr1);
		System.out.println("The given array is...");
		
		for(int x : arr1){
		
			System.out.print(x + " ");
		}
		System.out.println();

		System.out.println("Enter size of the 2nd array");
                int size2 = Integer.parseInt(br.readLine());

                int arr2[] = new int[size2];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr2.length; i++){

                        arr2[i] = Integer.parseInt(br.readLine());
                }

		Sorting(arr2);
                System.out.println("The given array is...");
                for(int y : arr2){

                        System.out.print(y + " ");
                }
                System.out.println();
		
		System.out.println("Enter the sum : ");
		int sum = Integer.parseInt(br.readLine());
		countPair(arr1,arr2,sum);
	}
}
