
// Last index of one :-
// eg-  i/p = 00001
//      o/p = 4

import java.io.*;
class Demo{

	static int Element(int arr[]){
	
		int i=0;
                int flag=0;
                for(i=arr.length-1; i>=0; i--){

                        if(arr[i]==1){
                                flag = 1;
                                break;
                        }
                }
		
		if(flag==1)
			return i;
		else
			return -1;
	
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements which is binary form ");

		int num = 0;
		for(int i=0; i<arr.length; i++){
		
			num  = Integer.parseInt(br.readLine());
			if(num==0 || num==1){
			
				arr[i] = num;
			}else{
			
				System.out.println("plz enter binary number");
				num  = Integer.parseInt(br.readLine());
				arr[i] = num;
			}
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		int idx = Element(arr);

		if(idx==-1)
			System.out.println("1 is not present ");
		else
			System.out.println("1 is present at index : " + idx);
	}
}
