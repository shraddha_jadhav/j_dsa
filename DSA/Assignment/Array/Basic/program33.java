
// Multiply left and right array sum
// eg-
// 	i/p = arr[] = {1,2,3,4}
// 	o/p = 21   [(1+2)*(3+4)]

import java.io.*;
class Demo{

	static int lrMulti(int arr[]){
	
		int mid = (arr.length)/2 ;

		int sum1 = 0;
		int sum2 = 0;

		for(int i=0; i<arr.length; i++){
		
			if(i < mid){
			
				sum1 = sum1 + arr[i];
			}else{
			
				sum2 = sum2 + arr[i];
			}
		}

		return sum1*sum2;
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		int ret = lrMulti(arr);

		System.out.println("The multiplication of left and right array sum is :" + ret);
	}
}
