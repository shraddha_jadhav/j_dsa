
// Maximum product of two numbers
// eg -
//      i/p :- arr[] = {1,4,3,6,7,0}
//      o/p :- 42

import java.io.*;
class Demo{

	static void maxProduct(int arr[]){
	
		int max1 = arr[0];
		int max2 = 0;

		for(int i=0; i<arr.length; i++){
		
			if(arr[i] > max1 || arr[i] > max2){
			
				max2 = max1;
				max1 = arr[i];
			}
		}

		System.out.println("maximum product of two numbers are : " + max1*max2);
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();

		maxProduct(arr);
	}
}
