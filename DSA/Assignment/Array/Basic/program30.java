
// Find the closest number
// eg- 
// 	i/p :- arr[] = {1,3,6,7}
// 		k = 4
// 	o/p :-  3 

import java.io.*;
class Demo{

	static void closestNum(int arr[], int k){
	
		if(arr[0] > k){
		
			System.out.println("The closest number of " + k + " is " + arr[0]);
		
		}else if(arr[arr.length-1] < k){
		
			System.out.println("The closest number of " + k + " is " + arr[arr.length-1]);
		}else{
		
			for(int i=1; i<arr.length; i++){
			
				if(arr[i-1] < k && arr[i] > k){
				
					if(k - arr[i-1] < arr[i]-k){
					
						System.out.println("The closest number of " + k + " is " + arr[i-1]);
					}else{
					
						System.out.println("The closest number of " + k + " is " + arr[i]);
					}
				}
			}
		}
	}

	static void Sorting(int arr[]){
	
		for(int i=1; i<arr.length; i++){
		
			int val = arr[i];
			int j = i-1;

			while(j>=0 && arr[j]>val){
			
				arr[j+1] = arr[j];
				j--;
			}
			arr[j+1] = val;
		}
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr1[] = new int[size];

		System.out.println("Enter array Elements");

		for(int i=0; i<arr1.length; i++){
		
			arr1[i] = Integer.parseInt(br.readLine());
		}
		
		Sorting(arr1);
		System.out.println("The given array is...");
		
		for(int x : arr1){
		
			System.out.print(x + " ");
		}
		System.out.println();

		System.out.println("Enter the number for finding their closest number");
		int k = Integer.parseInt(br.readLine());

		closestNum(arr1, k);
		
	}
}
