
// Find Unique element:-
// Given an array of size n which contains all elements occuring in multiple of k.
// except one element which doesn't occure in multiple of k. find that unique element.

import java.io.*;
class Demo{

	static int fun(int arr[], int k){
	
		for(int i=0; i<arr.length; i++){
		
			int count = 0;
			for(int j=0; j<arr.length; j++){
			
				if(arr[i]==arr[j]){
				
					count++;
				}
			}

			if(count%k != 0){
				return arr[i];
			}
		}

		return -1;
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Enter the K");
		int K = Integer.parseInt(br.readLine());

		System.out.println("Enter array Elements all elements are occurred in multiples of K except one");

		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The given array is...");
		
		for(int x : arr){
		
			System.out.print(x + " ");
		}
		System.out.println();	

		int ret = fun(arr,K);
		System.out.println("The unique element is a "+ ret);
	}
}
