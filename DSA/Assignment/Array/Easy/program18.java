
// Move all zero to the end of the array

import java.io.*;
class Demo{

	static void MoveArr(int arr[]){
	
		int start = 0;
		int end = arr.length-1;
		int temp = 0;

		while(start < end){
		
			if(arr[start]==0 && arr[end] != 0){
			
				temp = arr[start];
				arr[start] = arr[end];
				arr[end] = temp;
				end--;
			}

			start++;

			if(arr[end]==0)
				end--;
		}
	}
        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		MoveArr(arr);

		System.out.println("The given array after rotating...");

                for(int y : arr){

                        System.out.print(y + " ");
                }
                System.out.println();

        }
}

