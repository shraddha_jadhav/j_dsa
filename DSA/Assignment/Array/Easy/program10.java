
// first repeating number index
// eg-
// 	i/p:- arr[] = {1,5,3,4,3,5,6}

import java.io.*;
class Demo{

	static int firstRepeat(int arr[]){
	
		for(int i=0; i<arr.length; i++){
		
			for(int j=i+1; j<arr.length; j++){
			
				if(arr[i]==arr[j])
					return i;
			}
		}

		return -1;
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		int ret = firstRepeat(arr);

		if(ret==-1){
		
			System.out.println("There is no any repeating number");
		}else{
		
			System.out.println("The first repeating number is at index is "+ ret + " which is "+ arr[ret]);
		}
        }
}

