
// Three Greate Candidates

import java.io.*;
class Demo{

	static int GreatCandidates(int arr[]){
	
		int max1 = arr[0];
		int max2 = arr[1];
		int max3 = arr[2];

		for(int i=0; i<arr.length; i++){
		
			if(arr[i] > max1 || max1 > max2 || max2 > max3){
			
				max3 = max2;
				max2 = max1;
				max1 = arr[i];
			}
		}

		return max1*max2*max3;
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter the candidates ability in Integer form ");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given candidates ability is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		int ret = GreatCandidates(arr);
		System.out.println("The Three Greates candidates ability is : " + ret);
        }
}

