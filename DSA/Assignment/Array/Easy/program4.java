
// Count pairs with given elements

import java.io.*;
class Demo{

	static void Pairs(int arr[], int sum){
	
		int count = 0;
		for(int i=0; i<arr.length; i++){
		
			for(int j=i+1; j<arr.length; j++){
			
				if(arr[i]+arr[j]==sum)
					count++;
			}
		}

		System.out.println("The count of pairs with given sum is : "+ count);
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		System.out.println("Enter the sum ");
		int sum = Integer.parseInt(br.readLine());

		Pairs(arr,sum);
        }
}

