
// Even and odd

import java.io.*;
class Demo{

	static void EvenOdd(int arr[]){
	
		int temp1 = 0;
		int temp2 = 0;

		for(int i=0; i<arr.length-1; i++){
		
			if(i%2==0 && arr[i]%2==1){
			
				temp1 = arr[i];
				arr[i] = arr[i+1];
				arr[i+1] = temp1;
			}

			if(i%2==1 && arr[i]%2==0){
			
				temp2 = arr[i];
				arr[i] = arr[i+1];
				arr[i+1] = temp2;
			}
		}
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array (must be even number enter)");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements (equal no. of odd and even numbers)");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		EvenOdd(arr);

		System.out.println("The  array is...");

                for(int y : arr){

                        System.out.print(y + " ");
                }
                System.out.println();

        }
}

