
// Find all pairs with given sum

import java.io.*;
class Demo{

	static void Pairs(int arr1[], int arr2[], int sum){
	
		for(int i=0; i<arr1.length; i++){
		
			for(int j=0; j<arr2.length; j++){
			
				if(arr1[i]+arr2[j]==sum)
					System.out.println("(" + arr1[i] + "," + arr2[j] + ")");
			}
		}
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the 1st array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		System.out.println("Enter size of the 2nd array");

                int size2 = Integer.parseInt(br.readLine());

                int arr2[] = new int[size2];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr2.length; i++){

                        arr2[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int y : arr2){

                        System.out.print(y + " ");
                }
                System.out.println();

		System.out.println("Enter the given sum");
		int sum = Integer.parseInt(br.readLine());
		Pairs(arr,arr2,sum);
        }
}

