
// Remove Duplicates elements from the sorted array
// eg-
// 	i/p :- arr[] ={1,2,2,4}
// 	o/p :- 1 2 4

import java.io.*;
class Demo{

	static void printDistinctEle(int arr[]){

		System.out.println("After removing duplicates elements :");	
		System.out.print(arr[0] + " ");
		for(int i=1; i<arr.length; i++){
		
			if(arr[i]>arr[i-1])
				System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	static void Sorting(int arr[]){
	
		for(int i=0; i<arr.length; i++){
		
			for(int j=0; j<arr.length-i-1; j++){
			
				if(arr[j] > arr[j+1]){
				
					int temp = arr[j+1];
					arr[j+1] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

		Sorting(arr);

                System.out.println("The given array is sorted array...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		printDistinctEle(arr);
        }
}

