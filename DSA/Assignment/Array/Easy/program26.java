
// Leaders in an array
// eg-
//      i/p :- arr[] = {16,17,4,3,5,2}
//      o/p :- 17 5 2

import java.io.*;

class Demo{

        static void Leaders(int arr[]){

                System.out.println("The Leaders in the array is...");

                for(int i=0; i<arr.length-1; i++){

                        int flag = 0;
                        for(int j=i+1; j<arr.length; j++){

                                if(arr[i] < arr[j]){
                                        flag=1;
                                        break;
                                }
                        }

                        if(flag==0)
                                System.out.print(arr[i] + " ");
                }

                System.out.println(arr[arr.length-1]+ " ");

        }

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
		}
		System.out.println();

		Leaders(arr);

	}
}
