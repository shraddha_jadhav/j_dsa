
// Left most and right most Index
// eg-
// 	i/p :- arr[] = {1,3,5,5,5,5,67,123,125}
// 		x = 5;
// 	o/p :- 2 5

import java.io.*;
class Demo{

	static int BinarySearch(int arr[], int start, int end, int x){
	
		while(start <= end){
		
			int mid = (start+end)/2;

			if(arr[mid]==x){
			
				return mid;
			
			}else if(arr[mid] > x){
			
				return BinarySearch(arr,start, mid-1,x);
			}else{
			
				return BinarySearch(arr,mid+1,end, x);
			}
		}

		return -1;
	}

	static void LRmost(int arr[], int x){
	
		int idx = BinarySearch(arr,0,arr.length-1,x);

		if(idx==-1)
                        System.out.println("No such a element is present");

                int left = idx -1;
                int right = idx +1;

                while(left >=0 && arr[left]==x){

                        left--;
                }

		int l = left +1;
		System.out.println("The left most Inedx of "+ x + " is " + l);

                while(right <arr.length && arr[right]==x){

                        right++;
                }

		int r = right-1;
		System.out.println("The right most Index of " + x + " is " + r);
	}

	static void Sorting(int arr[]){
	
		for(int i=0; i<arr.length; i++){
		
			for(int j=0; j<arr.length-i-1; j++){
			
				if(arr[j] > arr[j+1]){
				
					int temp = arr[j+1];
					arr[j+1] = arr[j];
					arr[j]  = temp;
				}
			}
		}
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

		Sorting(arr);

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		System.out.println("Enter the element");
		int search = Integer.parseInt(br.readLine());
		LRmost(arr,search);
        }
}

