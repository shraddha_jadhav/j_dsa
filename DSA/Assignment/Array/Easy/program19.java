
// Elements with left side smaller right side greater

import java.io.*;
class Demo{

	static void Elements(int arr[]){

		System.out.println("The elements with left side smaller and right side greater is");

		int flag = 0;
		for(int i=1; i<arr.length-1; i++){
		
			if(arr[i-1] < arr[i] && arr[i] < arr[i+1]){
			
				System.out.print(arr[i] + " ");
				flag=1;
			}
		}
		System.out.println();

		if(flag==0)
			System.out.println("No such a elemet are present in the given array");
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		Elements(arr);
        }
}

