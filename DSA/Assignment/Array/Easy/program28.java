
// Product Array Puzzle

import java.io.*;
class Demo{

	static void Puzzle(int arr[]){
	
		int product = 1;
		for(int i=0; i<arr.length; i++){
		
			product = product*arr[i];
		}

		int arr2[] = new int[arr.length];

		for(int i=0; i<arr2.length; i++){
		
			arr2[i] = product/arr[i];
		}

		System.out.println("Product array Puzzle");

		for(int i=0; i<arr2.length; i++){
		
			System.out.print(arr2[i] + " ");
		}
		System.out.println();
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		Puzzle(arr);
        }
}

