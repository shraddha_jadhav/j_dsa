
// Facing the sun
// eg-
// 	i/p :- H[] = {7,4,8,2,9}
// 	o/p :- 3 

import java.io.*;
class Demo{

	static int Sunrise(int arr[]){
	
		int max = arr[0];
		int count = 1;

		for(int i=1; i<arr.length; i++){
		
			if(arr[i] > max){
			
				count++;
				max = arr[i];
			}
		}

		return count;
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the number of the Buildings");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter the height of each building");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given Buildings hight is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		int ret = Sunrise(arr);
		System.out.println("The count of buildings which will see the sunrise is : " + ret );
        }
}

