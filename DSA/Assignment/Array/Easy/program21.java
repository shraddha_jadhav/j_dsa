
// Non-Repeating Element
// eg-
// 	i/p :- arr[] = {-1,2,-1,3,2}
// 	o/p :- 3

import java.io.*;
class Demo{

	static void NonRepeat(int arr[]){
	
		int min = arr[0];
		for(int i=0; i<arr.length; i++){
		
			if(min > arr[i])
				min = arr[i];
		}

		int min2 = 0;
		if(min < 0){
		
			 min2 = -min;
			for(int i=0; i<arr.length; i++){
			
				arr[i] += min2;
			}

		}
		
		int max = arr[0];
		for(int i=1; i<arr.length; i++){
		
			if(arr[i] > max)
				max = arr[i];
		}

		int countArr[] = new int[max+1];

		for(int i=0; i<arr.length; i++){
		
			countArr[arr[i]]++;
		}

		System.out.print("The Non Repeating numbers are : ");

		for(int i=0; i<countArr.length; i++){
		
			if(countArr[i]==1)
				System.out.print(i-min2 + " ");
		}
		System.out.println();
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		NonRepeat(arr);
        }
}

