
// Bitonic point
// eg-
// 	i/p :- arr[] = {1,15,25,45,42,21,17,12,11}
// 	o/p :- 45

import java.io.*;
class Demo{

	static int BitonicPoint(int arr[]){
	
		int max = arr[0];
		for(int i=1; i<arr.length; i++){
		
			if(arr[i] >max)
				max = arr[i];
		}

		return max;
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		int ret = BitonicPoint(arr);
		System.out.println("Bitonic point is "+ ret);
        }
}

