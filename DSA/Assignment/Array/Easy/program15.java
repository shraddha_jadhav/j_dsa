
// Union of two sorted Array

import java.io.*;
class Demo{

	static void Union(int arr1[], int arr2[]){
	
		int max = arr1[0];

		for(int i=0; i<arr1.length; i++){
		
			if(arr1[i] > max)
				max = arr1[i];
		}

		for(int i=0; i<arr2.length; i++){
		
			if(arr2[i] > max)
				max = arr2[i];
		}

		int countArr[] = new int[max+1];

		for(int i=0; i<arr1.length; i++){
		
			countArr[arr1[i]]++;
		}

		for(int i=0; i<arr2.length; i++){
		
			countArr[arr2[i]]++;
		}
		
		// Union of two sorted array
		System.out.println("Union of two sorted array is...");
		for(int i=0; i<countArr.length; i++){
		
			if(countArr[i] > 0){
			 
				System.out.print(i + " ");
			}
		}
		System.out.println();
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the 1st array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		System.out.println("Enter size of the 2nd array");

                int size2 = Integer.parseInt(br.readLine());

                int arr2[] = new int[size2];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr2.length; i++){

                        arr2[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int y : arr2){

                        System.out.print(y + " ");
                }
                System.out.println();
		
		Union(arr,arr2);
        }
}

