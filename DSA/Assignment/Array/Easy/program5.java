
// Peak element

import java.io.*;
class Demo{

	static int PeakEle(int arr[]){
	
		if(arr[0] > arr[1]){
		
			return 0;
		}else{
		
			for(int i=1; i<arr.length-1; i++){
			
				if(arr[i] > arr[i+1] && arr[i] > arr[i-1]){
				
					return i;
				}
			}

			if(arr[arr.length-1]>arr[arr.length-2]){
			
				return arr.length-1;
			}
		}

		return -1;
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		int ret = PeakEle(arr);

		if(ret==-1){
		
			System.out.println("There is no any peak element");
		}else{
			System.out.println("Peak element index is " + ret + " and which is " + arr[ret]);
		}
        }
}

