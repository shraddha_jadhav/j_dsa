
// Check if array is sorted or not

import java.io.*;
class Demo{

	static boolean Sorted(int arr[]){
	
		for(int i=1; i<arr.length-1; i++){
		
			if(arr[i-1] > arr[i] && arr[i+1] > arr[i]){
			
				return false;
			
			}
		       	if(arr[i-1] < arr[i] && arr[i+1] < arr[i]){
			
				return false;
			}
		}

		return true;
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		boolean ret = Sorted(arr);
		
		if(ret==true){
		
			System.out.println("the given array is sorted");
		}else{
		
			System.out.println("The given array is unsorted");
		}
        }
}

