
// Rotate Array

import java.io.*;
class Demo{

	static void RotateArr(int arr[], int rot){
	
		while(rot>0){
		
			int val = arr[0];

			int i=1;

			while(i <= arr.length-1){
			
				arr[i-1] = arr[i];
				i++;
			}

			arr[i-1] = val;
			rot--;
		}
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		System.out.println("enter the value for rotating the array that's times");
		int rot = Integer.parseInt(br.readLine());

		RotateArr(arr,rot);

		System.out.println("After rotate the " + rot + " times");

		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i] + " ");
		}
		System.out.println();
        }
}

