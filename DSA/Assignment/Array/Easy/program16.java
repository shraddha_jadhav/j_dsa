
// Rotation
// how many times roate givan sorted array

import java.io.*;
class Demo{

	static int Rotate(int arr[]){
	
		int min = arr[0];

		int store = 0;
		for(int i=0; i<arr.length; i++){
		
			if(arr[i] < min){
			
				min = arr[i];
				store = i;
			}
		}

		return store;
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is sorted rotated array");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		int ret = Rotate(arr);

		System.out.println("The given array is rotate " + ret + " times");
        }
}

