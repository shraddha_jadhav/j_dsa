
// Find Duplicates in an array

import java.io.*;
class Demo{

	static void Duplicates(int arr[]){
	
		int max = arr[0];

		for(int i=1; i<arr.length; i++){
		
			if(arr[i] > max)
				max = arr[i]; 
		}

		int countArr[] = new int[max+1];

		for(int i=0; i<arr.length; i++){
		
			countArr[arr[i]]++;
		}
		
		System.out.println("The duplicates of the elements are...");
		int flag=0;
		for(int i=0; i<countArr.length; i++){
		
			if(countArr[i] > 1){
				flag = 1;
				System.out.print(i + " ");
			}
		}
		System.out.println();
		
		if(flag==0)
			System.out.println("There is no any duplicate elements");
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		Duplicates(arr);
        }
}

