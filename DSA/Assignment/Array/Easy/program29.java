
// Key pair

import java.io.*;
class Demo{

	static boolean keyPair(int arr[], int sum){
	
		for(int i=0; i<arr.length; i++){
		
			for(int j=i+1; j<arr.length; j++){
			
				if(arr[i]+arr[j]==sum)
					return true;
			}
		}
		return false;
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		System.out.println("Enter the sum to find the pair");
		int sum = Integer.parseInt(br.readLine());

		boolean ret = keyPair(arr,sum);

		if(ret==true)
			System.out.println("The given sum's pairs exist (yes)");
		else
			System.out.println("There is no such pair is exist (No)");
        }
}

