
// missing number in the array
// eg-
// 	i/p:- N=6, arr[] = {1,2,4,5,6}
// 	o/p:- 3

import java.io.*;
class Demo{

	static void missing(int arr[], int N){
	
		int countArr[] = new int[N+2];

		for(int i=0; i<arr.length; i++){
		
			countArr[arr[i]]++;
		}

		for(int i=1; i<countArr.length; i++){
		
			if(countArr[i]==0){
			
				System.out.println("missing element is : " + i);
			}
		}
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		missing(arr,size);
        }
}
