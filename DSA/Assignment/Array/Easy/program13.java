
// Minimum distance between two elements
// eg-
// 	i/p :- arr[] = {1,2,3,3}
// 		x = 1, y=2
// 	o/p :- 1

import java.io.*;
class Demo{

	static int Distance(int arr[], int first, int sec){
	
		int idx1 = 0;
		int idx2 = -1;
		int flag1 = 0;
		int flag2 = 0;
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]==first){
				flag1 =1;
				idx1 = i;
			}

			if(arr[i]==sec){
				flag2 = 1;
				idx2 = i;
			}
			if(flag1==1 && flag2==1)
				break;
		}

		return idx2-idx1;

	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		System.out.println("enter the 1st element in the array");
		int first = Integer.parseInt(br.readLine());

		System.out.println("enter the 2nd element in the array");
                int sec = Integer.parseInt(br.readLine());

		int ret = Distance(arr, first, sec);
		if(ret==-1){
		
			System.out.println("No such element is present");
		}else{
			System.out.println("The distance between two " + first + " and " + sec + " is " + ret);
		}
        }
}

