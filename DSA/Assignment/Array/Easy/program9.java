
// Find transition point
// Given an sorted array containing only 0's and 1's. find transition point
// eg-
// 	i/p:- arr[] = {0,0,0,1,1}
// 	o/p:- 3

import java.io.*;
class Demo{

	static void Sorting(int arr[]){
	
		for(int i=0; i<arr.length; i++){
		
			for(int j=0; j<arr.length-i-1; j++){
			
				if(arr[j+1]<arr[j]){
				
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1]= temp;
				}
			}
		}
	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements in the binary form ");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

		Sorting(arr);
                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		int flag=0;
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]==1 && arr[i-1]<1){
			
				System.out.println("the transition point is " + i);
				flag=1;
			}
		}

		if(flag==0)
			System.out.println("There is no transition point");
        }
}

