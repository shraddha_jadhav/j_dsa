
// Frequencies of limited Range Array elements
// eg-
// 	i/p :- arr[] = {2,3,2,3,5}
// 	o/p :- 0 2 2 0 1

import java.io.*;
class Demo{

	static void Frequencies(int arr[]){
	
		int max = arr.length;

		int countArr[] = new int[max+1];

		for(int i=0; i<arr.length; i++){
		
			countArr[arr[i]]++;
		}

		System.out.println("frequencies of the array elements is...");

		for(int i=1; i<countArr.length; i++){
		
			System.out.print(countArr[i] + " ");
		}
		System.out.println();


	}

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of the array");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements");

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("The given array is...");

                for(int x : arr){

                        System.out.print(x + " ");
                }
                System.out.println();

		Frequencies(arr);
        }
}

